<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\ToDoList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Http\Request;

class ToDoItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ToDoList::paginate(10);
      
        return $this->sendResponse($items, '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();
        $data = ToDoList::create($input);
        $success['data'] =  $data;

        return $this->sendResponse($success, 'Todo item created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ToDoList  $toDoItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $item = TodoList::where('id',$id)->first();
       return $this->sendResponse($item, '');

    }
    
        /**
     * Search the specified resource.
     *
     * @param  \App\Models\ToDoList  $toDoItem
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = $request->search;
        $items  = ToDoList::where('title','LIKE','%'.$search.'%')->paginate(10);
       return $this->sendResponse($items, '');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ToDoList  $toDoItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ToDoItem $toDoItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ToDoList  $toDoItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ToDoList $toDoItem)
    {
        TodoList::where('id',$request->id)->update(['title'=>$request->title,'description'=>$request->description]);
       return $this->sendResponse($toDoItem, '');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ToDoList  $toDoItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TodoList::where('id',$id)->delete();
        return $this->sendResponse("", 'Deleted Successfully.');
    }

     /**
     * Write code on Method
     *
     * @return response()
     */
    public function logout(Request $request) {
        $request->user()->currentAccessToken()->delete();
        return $this->sendResponse("", 'User Logout Successfully.');    }
}
