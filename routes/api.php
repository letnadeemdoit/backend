<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(\App\Http\Controllers\API\AuthController::class)->group(function(){
    Route::post('register', 'register');
    Route::post('login', 'login');
});

Route::middleware('auth:sanctum')->controller(\App\Http\Controllers\API\ToDoItemController::class)->group(function(){
    Route::get('todolist', 'index');
    Route::post('addtodo', 'store');
    Route::post('updatetodo', 'update');
    Route::get('viewtetodo/{id}', 'show');
    Route::delete('deletetetodo/{id}', 'destroy');
    Route::post('todosearch', 'search');
    Route::post('logout', 'logout');
});
